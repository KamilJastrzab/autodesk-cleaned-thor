﻿using UnityEngine;
using System.Collections.Generic;


public class ActivateWidget : MonoBehaviour {

	public List<GameObject> widgetsToActivateNext;
	public List<GameObject> widgetsToDeactivate;

	void OnActivateButton()
	{
		foreach (GameObject widget in widgetsToActivateNext) {
			widget.SetActive(true);
		}
		foreach (GameObject widget in widgetsToDeactivate) {
			Debug.Log ("Deactivating " + widget.name);
			widget.SetActive(false);
		}
	}
}
