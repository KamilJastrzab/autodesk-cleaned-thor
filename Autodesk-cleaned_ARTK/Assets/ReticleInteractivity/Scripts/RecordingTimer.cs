﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RecordingTimer : MonoBehaviour {

	private Text text;
	public float timerLength = 6.0f;
	public float currTimer;
	private int counter;
	private string secsInString;
	private bool didBroadcastMessage;
	// Use this for initialization
	void OnEnable () {
	
		text = GetComponent<Text> ();
		currTimer = 0.0f;
		counter = 0;
		didBroadcastMessage = false;
		secsInString = "0:0" + currTimer + "/" + "0:0" + timerLength;
	}
	
	// Update is called once per frame
	void Update () {

		if (currTimer < timerLength) {
			counter = (int)currTimer;
			currTimer += Time.deltaTime;
			secsInString = "0:0" + counter + "/" + "0:0" + timerLength;
			text.text = "\nRec. / Total\n" + secsInString;
		} else if(!didBroadcastMessage){
			BroadcastMessage("OnActivateButton");
			didBroadcastMessage = true;
		}
	
	}

}
