﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ActivateWidget))]
public class WaitAndActivate : MonoBehaviour {

	public float secs;

	// Use this for initialization
	void OnEnable () {
	
		StartCoroutine (WaitAndSendBroadcastMessage ());
	}
	
	private IEnumerator WaitAndSendBroadcastMessage()
	{
		yield return new WaitForSeconds(secs);

		BroadcastMessage ("OnActivateButton");
	}
}
