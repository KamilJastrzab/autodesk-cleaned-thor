using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum ReticleState { RELEASE, ATTACH };

public class ReticleFollower : MonoBehaviour {

	public static ReticleFollower Instance
	{
		get{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType(typeof(ReticleFollower)) as ReticleFollower;
			}
			return _instance;
		}
	}

	private static ReticleFollower _instance;
	private ReticleState currState;

	public Transform reticleRingTransform;
	public float lockSpeed = 1000.0f;
	public Camera cam;
	public AudioClip activateSound;
	public AudioClip deactivateSound;

	[HideInInspector] public ReticleSelectableItem followObject;
	[HideInInspector] public bool centerMe = true;
	
	private Vector3 reticleDefaultPosition;
	private Vector3 reticleDefaultScale;
	private Vector3 reticleDefaultRotation;
	private bool activateSoundHasPlayed = false;
	private bool deactivateSoundHasPlayed = false;
	
	
	public void UpdateState(ReticleState state)
	{
		currState = state;
	}

	void Start () {

		SetReticleRenderer(true);

		reticleDefaultPosition = reticleRingTransform.localPosition;
		reticleDefaultScale = reticleRingTransform.localScale;
		reticleDefaultRotation = reticleRingTransform.localEulerAngles;
		currState = ReticleState.ATTACH;

	}
	
	public void SetReticleRenderer(bool enabled) {
		Renderer[] renderers = reticleRingTransform.GetComponentsInChildren<Renderer>();
		foreach (Renderer aRenderer in renderers) {
			aRenderer.enabled = enabled;
		}
	}


	private void KeepReticleCentered()
	{
		Vector3 screenToWorldCenter = cam.ScreenToWorldPoint (new Vector3 (Screen.width / 2, Screen.height / 2, Camera.main.nearClipPlane + 2));
		if (!Mathf.Approximately (transform.position.x, screenToWorldCenter.x) || 
		    !Mathf.Approximately (transform.position.y, screenToWorldCenter.y) || 
		    !Mathf.Approximately (transform.position.z, screenToWorldCenter.z)
		    ) {
			transform.position = Vector3.MoveTowards (transform.position, screenToWorldCenter, Time.deltaTime * lockSpeed);
		} else {
			transform.position = screenToWorldCenter;
		}


		transform.rotation = Quaternion.Lerp(transform.rotation, cam.transform.rotation, Time.deltaTime * 6.0f);
	}

	private void ResetReticleTransform()
	{
		reticleRingTransform.localPosition = Vector3.Lerp(reticleRingTransform.localPosition, reticleDefaultPosition, Time.deltaTime * lockSpeed);
		reticleRingTransform.localScale = Vector3.Lerp(reticleRingTransform.localScale, reticleDefaultScale, Time.deltaTime * lockSpeed * .2f);
		reticleRingTransform.localEulerAngles = Vector3.Lerp(reticleRingTransform.localEulerAngles, reticleDefaultRotation, Time.deltaTime * lockSpeed);
	}

	bool playedSound;

	private void PlaySound(AudioClip clip)
	{
		if (!playedSound) {
			playedSound = true;
			GetComponent<AudioSource>().PlayOneShot(clip);
		}
	}

	void LateUpdate() {

	switch (currState) {
		case ReticleState.ATTACH: 

			PlaySound (deactivateSound);
			
			KeepReticleCentered ();
			
			ResetReticleTransform ();
			break;

		case ReticleState.RELEASE:
			if (followObject != null) {
				PlaySound (activateSound);
				
				transform.position = Vector3.MoveTowards (transform.position, followObject.transform.position, Time.deltaTime * lockSpeed * 1.5f);
				
				//lerp position
				reticleRingTransform.localPosition = Vector3.Lerp (reticleRingTransform.localPosition, Vector3.zero, Time.deltaTime * lockSpeed);	
				
				//lerp scale
				Transform OGParent = reticleRingTransform.parent;
				reticleRingTransform.parent = followObject.transform;
				reticleRingTransform.localScale = Vector3.Lerp (reticleRingTransform.localScale, Vector3.one, Time.deltaTime * lockSpeed * 3);
				reticleRingTransform.parent = OGParent;
				
				reticleRingTransform.localEulerAngles = Vector3.Lerp (reticleRingTransform.localEulerAngles, Vector3.zero, Time.deltaTime * lockSpeed);
			}
			break;
		}
	}

}