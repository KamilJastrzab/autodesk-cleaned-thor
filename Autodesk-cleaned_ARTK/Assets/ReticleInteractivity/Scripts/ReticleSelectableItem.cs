﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(ActivateWidget))]
public class ReticleSelectableItem : MonoBehaviour {

	public event System.Action<ReticleSelectableItem> ReleaseReticle;
	public event System.Action<ReticleSelectableItem> AttachReticle;

	public bool ActivateOnce;
	private bool didActivate;
	private bool activationDelayStarted = false;
	private float delayStartTime;
	private Animator timerAnimator;

	private float addDelayToActivation = 1.0f;
	private float hasBeenActiveFor;

	void OnEnable () {

		hasBeenActiveFor = 0.0f;

		activationDelayStarted = false;

		Transform timerSprite = transform.FindChild ("TimerSprite") as Transform;
		timerAnimator = timerSprite.GetComponent<Animator> () as Animator;

		ToggleFollowReticleRenderer(true);
	}
	
	void Update () {

		if (activationDelayStarted == true) {
			float curTime = Time.time;
			if (curTime > delayStartTime) {
				activationDelayStarted = false;
				SetReticleFollow();
				Activate ();
			}
		}

		hasBeenActiveFor += Time.deltaTime;
	}
	
	void OnTriggerEnter(Collider hit) {

		if (hasBeenActiveFor <= addDelayToActivation)
			return;

		if (hit.transform.name == "Reticle Trigger") {
			delayStartTime = Time.time;
			activationDelayStarted = true;
		}
	}
	
	void OnTriggerExit(Collider hit) {
		if (hit.transform.name == "Reticle Trigger") {
			activationDelayStarted = false;
			ReleaseReticleFollow();
			Deactivate();
		}
	}

	void Activate() {

		if (ActivateOnce && didActivate)
			return;

		if (timerAnimator != null) {
			TimerAnimationEvents component = timerAnimator.GetComponent<TimerAnimationEvents>();
			component.PlayClip();

			timerAnimator.transform.localPosition = Vector3.zero;
			timerAnimator.Play ("AnimSpriteCanvas");



		}
	}
	public void Deactivate() {
		TimerAnimationEvents component = timerAnimator.GetComponent<TimerAnimationEvents>();
		component.StopClip ();
		Debug.Log ("Deactivating...");
		if (timerAnimator != null) {
			timerAnimator.Play("TimerDefault");

		}
	}

	public void TimerFinished () {
		activationDelayStarted = false;
		ReleaseReticleFollow ();
		timerAnimator.Play ("TimerDefault");

		BroadcastMessage("OnActivateButton");
		didActivate = true;
	}


	void ToggleFollowReticleRenderer(bool enabled) {
		ReticleFollower.Instance.SetReticleRenderer (enabled);
	}
	
	void SetReticleFollow() {

		ReticleFollower.Instance.UpdateState (ReticleState.RELEASE);
		ReticleFollower.Instance.followObject = this;
		
	}
	
	void ReleaseReticleFollow() {
		ReticleFollower.Instance.UpdateState (ReticleState.ATTACH);
	}


}
