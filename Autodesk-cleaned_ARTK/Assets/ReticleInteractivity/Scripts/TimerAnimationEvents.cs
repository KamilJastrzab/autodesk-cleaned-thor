﻿using UnityEngine;
using System.Collections;


public class TimerAnimationEvents : MonoBehaviour {
	
	private ReticleSelectableItem selectableItemScript;
	public bool TimerAnimationOver;
	public AudioClip clip;
	private AudioSource source;

	void OnEnable()
	{
		source = GetComponent<AudioSource> ();
	}

	public void PlayClip()
	{
		source.PlayOneShot (clip);
	}

	public void StopClip()
	{
		source.Stop();
	}

//	void Start () {
//		if (transform.parent.gameObject.GetComponent<ReticleSelectableItem>() != null) {
//			selectableItemScript = transform.parent.gameObject.GetComponent<ReticleSelectableItem>();
//		}
//
//	}
//
//	void TimerFinished () {
//		if (selectableItemScript != null) {
//			selectableItemScript.TimerFinished();
//		}
//	}

	void Update()
	{
		if (TimerAnimationOver) {
			if (transform.parent.gameObject.GetComponent<ReticleSelectableItem>() != null) {
				selectableItemScript = transform.parent.gameObject.GetComponent<ReticleSelectableItem>();
				selectableItemScript.TimerFinished();
				TimerAnimationOver = false;
			}
		}
	}



}
