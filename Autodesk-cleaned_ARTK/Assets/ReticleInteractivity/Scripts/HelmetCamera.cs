﻿// © 2015 DAQRI, LLC
// ALL RIGHTS RESERVED

using UnityEngine;

[RequireComponent(typeof(Camera))]
public class HelmetCamera : MonoBehaviour {

	public enum HelmetType { MK14, MK17};

	public HelmetType helmetType;
	//[SerializeField]

	private Camera _camera = null;
    private float verticalFOV = 18.4f;
    
	private void Start() {
		// Making cached access to camera.
		_camera = GetComponent<Camera>();
        
	}

	private void LateUpdate() {
        
        RotateCameraFixFOV();
	}
    
    
    float getVerticalFov(float diagonalFOV)
    {
        return (2.0f*Mathf.Atan(Mathf.Tan(diagonalFOV*Mathf.Deg2Rad/2.0f)/Mathf.Sqrt(1 + Mathf.Pow(_camera.aspect, 2.0f)))*Mathf.Rad2Deg);
    }


	//Use this for MK14 Calibration
	private static Matrix4x4 ROTATION_MATRIX = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(270f, Vector3.forward), Vector3.one);

	//this for MK17 Calibration
	private static readonly Matrix4x4 ROTATION_180_YAW = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(180f, Vector3.forward), Vector3.one);
	//this one alone brings us to 180 along yaw
	private static readonly Matrix4x4 ROTATION_BACK_CAMERA_FIX = Matrix4x4.TRS(Vector3.zero, Quaternion.AngleAxis(180f, Vector3.forward), Vector3.one);
	
    
	/// <summary>Sets the camera's project matrix to be rotated each time it changes.</summary>
	private void RotateCameraFixFOV() {

		if (helmetType == HelmetType.MK17) {
			ROTATION_MATRIX = ROTATION_180_YAW * ROTATION_BACK_CAMERA_FIX;
		}

		// Rotating the camera.
        Matrix4x4 fovMat = Matrix4x4.Perspective(verticalFOV,_camera.aspect, _camera.nearClipPlane, _camera.farClipPlane);

        if (!Application.isEditor)
        {
            _camera.projectionMatrix =  fovMat * ROTATION_MATRIX;
        }
        else
        {
            _camera.projectionMatrix =  fovMat * ROTATION_MATRIX;
        }
	}

    private void RotateCamera()
	{
		Matrix4x4 ROTATION_MATRIX = ROTATION_180_YAW * ROTATION_BACK_CAMERA_FIX;

        _camera.projectionMatrix = ROTATION_MATRIX;
    }

}
