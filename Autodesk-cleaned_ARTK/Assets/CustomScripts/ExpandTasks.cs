﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExpandTasks : MonoBehaviour {

	public Animation anim;
	public string expandAnim = "";
	public string collapseAnim = "";


	public GameObject plusIcon;
	public GameObject minusIcon;

	bool collapse;

	void OnActivateButton()
	{
		Debug.Log ("OnActivateButton");
		if (!collapse && expandAnim != null) {
			anim.Play (expandAnim);
		} else if(collapseAnim != null){
			anim.Play (collapseAnim);
		}

		collapse = !collapse;


		if (plusIcon != null) {
			plusIcon.SetActive(!collapse);
		}
		if (minusIcon != null) {
			minusIcon.SetActive(collapse);
		}
	
	}





}
