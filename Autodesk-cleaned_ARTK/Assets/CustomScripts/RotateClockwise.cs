﻿using UnityEngine;
using System.Collections;

public class RotateClockwise : MonoBehaviour 
{
	public float turnSpeed = 10f;
	public GameObject model;

	public bool rotate;

	void Update () 
	{
		if (rotate) {
			model.transform.Rotate (Vector3.up * turnSpeed * 10f * Time.deltaTime);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		rotate = true;
	}

	void OnTriggerExit(Collider col)
	{
		rotate = false;
	}


}
