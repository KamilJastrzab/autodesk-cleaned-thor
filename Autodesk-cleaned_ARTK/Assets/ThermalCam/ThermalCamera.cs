﻿using UnityEngine;
using System.Runtime.InteropServices;
using System.IO;

public class ThermalCamera : MonoBehaviour
{
    Texture2D texture;
    byte[] frame;

    void Awake()
    {
        if (!Application.isEditor)
        {
            Debug.LogWarning("ThermalCamera -- OnEnable()");
            int width, height;
            ThermalCamera_GetFrameDimensions(out width, out height);

            texture = new Texture2D(width, height, TextureFormat.ARGB32, false);
            frame = new byte[width * height * 4];
            GetComponent<Renderer>().material.mainTexture = texture;

            ThermalCamera_Start(FindCameraDevice());
        }
    }

	void OnApplicationQuit()
    {
        if (!Application.isEditor)
        {
            Debug.LogWarning("ThermalCamera -- OnDisable()");
            GetComponent<Renderer>().material.mainTexture = null;
            Destroy(texture);
            frame = null;

            ThermalCamera_Stop();
        }
    }

    void Update()
    {
        if (!Application.isEditor)
        {
            bool did_read = ThermalCamera_ReadFrame(frame);
            if (did_read)
            {
                var texture_colors = new Color32[texture.width * texture.height];

                for (int i = 0; i < texture_colors.Length; ++i)
                {
                    texture_colors[i] = new Color32(frame[i * 4], frame[i * 4 + 1], frame[i * 4 + 2], frame[i * 4 + 3]);
                }

                texture.SetPixels32(texture_colors);
                texture.Apply();
            }
        }
    }

    const string library = "thermalcamera";

    [DllImport(library)]
    static extern void ThermalCamera_Start(string path);

    [DllImport(library)]
    static extern void ThermalCamera_Stop();

    [DllImport(library)]
    static extern void ThermalCamera_GetFrameDimensions(out int width, out int height);

    [DllImport(library)]
    static extern bool ThermalCamera_ReadFrame(byte[] frame);

    static string FindCameraDevice()
    {
        var devices = Directory.GetFiles("/dev");

        foreach (var device in devices)
        {
            if (device.Contains("ACM"))
                return device;

            if (device.Contains("cu.usbmodem"))
                return device;
        }

        return null;
    }
}
