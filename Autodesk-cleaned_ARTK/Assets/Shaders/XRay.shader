﻿Shader "DAQRI/Core/XRay" {
	Properties {
		_MainTex 		 ( "Base (RGB)", 2D ) = "white" {}
		_TintColor 		 ( "Tint Color", Color ) = ( 1, 1, 1, 1 )
		_RimColor 		 ( "Rim Color", Color ) = ( 0.5,0.5,0.5,0.5 )
		_InnerColor 	 ( "Inner Color", Color ) = ( 0.5,0.5,0.5,0.5 )
		_InnerColorPower ( "Inner Color Power", Range( 0.0,1.0 ) ) = 0.5
		_RimPower 		 ( "Rim Power", Range( 0.0,5.0 ) ) = 2.5
		_AlphaPower 	 ( "Alpha Rim Power", Range( 0.0,8.0 ) ) = 4.0
		_AllPower 	  	 ( "All Power", Range( 0.0, 10.0 ) ) = 1.0
		_ScrollUSpeed 	 ( "U Scroll Speed", Range( 0, 100 ) ) = 0
		_ScrollVSpeed 	 ( "V Scroll Speed", Range( 0, 100 ) ) = 0
		_RimEffect 		 ( "Rim Effect", int ) = 1
		_Fader			 ( "Fader", Range( 0, 1 ) ) = 1.0
	}
	
	SubShader {
		Tags { "Queue" = "Transparent" }
		
//		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#pragma surface surf Lambert alpha
		
		struct Input {
			float3 viewDir;
			float2 uv_MainTex;
		};
		
		sampler2D _MainTex;
		float4 	  _TintColor;
		float4 	  _RimColor;
		float 	  _RimPower;
		float 	  _AlphaPower;
		float 	  _AlphaMin;
		float 	  _InnerColorPower;
		float 	  _AllPower;
		float4    _InnerColor;
		fixed     _ScrollUSpeed;
		fixed     _ScrollVSpeed;
		int	  	  _RimEffect;
		fixed	  _Fader;

		void surf( Input IN, inout SurfaceOutput o ) {
			fixed2 scrolledUV = IN.uv_MainTex;
			fixed uScrollValue = _ScrollUSpeed * _Time;
			fixed vScrollValue = _ScrollVSpeed * _Time;

			scrolledUV += fixed2( uScrollValue, vScrollValue );

			half4 c  = tex2D( _MainTex, scrolledUV ) * _TintColor;
			half rim = saturate( dot( normalize( IN.viewDir ), o.Normal ) );
			
			// rim or center effect
			if ( _RimEffect ) rim = 1.0 - rim;
			
			o.Emission = ( c.rgb * _RimColor.rgb * pow( rim, _RimPower ) * _AllPower + ( _InnerColor.rgb * 2 * _InnerColorPower ) ) * _Fader;
			o.Alpha    = c.a * ( pow( rim, _AlphaPower ) ) * _AllPower * _Fader;
		}
		
		ENDCG
	}
	
	Fallback "VertexLit"
}