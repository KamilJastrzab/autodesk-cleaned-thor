## Summary ##

This Unity project demonstrates a simulated experience of how the Smart Helmet adds value to Autodesk's BIM 360 product.

## User Journey ##

The demo experience ideates that the worker is at a building site. 

A worker wearing the DAQRI Smart Helmet at a construction site will perform a series of tasks to verify that a building plan change from the architectural office is feasible. The worker is using the DAQRI Smart Helmet with controls and functions inspired by Autodesk BIM 360. This process is completely hands-free.
The onsite worker will use the Smart Helmet to conduct a visual inspection of local systems for feasibility based on a building plan change from the architectural office. First, the worker will be notified of a building plan change and review his/her current task. Second, the worker will perform a side-by-side comparison of the listed task with the local 3D vis model of the effected system. Third, the worker will view and interact with the detailed local 3D model and perform the visual inspections that their task requires of them. If a plan clash is detected during the visual inspection, the worker will submit a plan clash report. Upon completion of the task, the building plan will be updated.

## Git Branch ##

Autodesk_U_Vuforia

## Key Unity Scene(s) ##

Assets/Scenes/ADSK_Vuforia_SingleTarget (Optimized SHOC version current as of 1-21-16)

Assets/Scenes/mk17_Vuforia (original 2-target extended experience)

## How to launch/run-through the experience ##

Unity: No special instructions other than adding the scene to build settings and running it on the specific platform (Android)

Experience Walkthrough: AR guidance panels appear at the beginning of each section of the experience describing what the user needs to do and how to do it. The Home Icon (top center) exits the app and goes to the launcher. The Reset icon (after Sync complete message will restart the experience)

## Developer Notes ##

- This app uses Vuforia for tracking and recognition.

## Point Of Contact ##

For any questions/concerns around running the app or queries related to the source code, contact Jayashree Nagarajan (jayashree.nagarajan@daqri.com) or Josh Morris (josh@daqri.com)